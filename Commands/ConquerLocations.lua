function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move each unit to a separate location. If more units than locations, start again from current unit and first location",
		parameterDefs = {
			{
				name = "unitGroup",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "locations",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
end

function Run(self, units, parameters)
	local locations = parameters.locations
	local unitGroup = parameters.unitGroup
	local cmdID = CMD.FIGHT
    local done = true
    for i=1,#unitGroup do
        local loci = i - math.floor((i-1)/#locations)*#locations
        local targetPosition = Vec3(locations[loci].x,locations[loci].y, locations[loci].z)
        local posx, posy, posz = SpringGetUnitPosition(unitGroup[i])
        local position = Vec3(posx, posy, posz)
        if (position:Distance(targetPosition) > 20) then
            SpringGiveOrderToUnit(unitGroup[i], cmdID, targetPosition:AsSpringVector(), {})
			done = false
		end
    end
    if (done) then
        return SUCCESS
    else
        return RUNNING
    end
end

function Reset(self)
	ClearState(self)
end
