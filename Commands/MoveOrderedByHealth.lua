function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "approach target at similar time, sorted by health, the highest goes first",
		parameterDefs = {
			{
				name = "unitGroup",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "location",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitHealth = Spring.GetUnitHealth

local function ClearState(self)
end

function Run(self, units, parameters)
	local location = parameters.location
    local unitGroup = parameters.unitGroup
	local cmdID = CMD.FIGHT
    local stopID = CMD.STOP

    local targetPosition = Vec3(location.x,location.y, location.z)
    local chief = unitGroup[1]
    local chiefHP = SpringGetUnitHealth(chief)
    for i=1,#unitGroup do
        local health = SpringGetUnitHealth(unitGroup[i])
        if (health > chiefHP) then
            chief = unitGroup[i]
            chiefHP = health
        end
    end
    local cposx, cposy, cposz = SpringGetUnitPosition(chief)
    local chiefPosition = Vec3(cposx, cposy, cposz)
    local chiefDistance = chiefPosition:Distance(targetPosition)


    local groupDistanceFromChief = 0
    for i=1,#unitGroup do
        if (unitGroup[i] ~= chief) then
            local posx, posy, posz = SpringGetUnitPosition(unitGroup[i])
            local position = Vec3(posx, posy, posz)
            local distance = position:Distance(targetPosition)
            groupDistanceFromChief = groupDistanceFromChief + distance - chiefDistance
            if (distance < chiefDistance) then
                SpringGiveOrderToUnit(unitGroup[i], stopID, {}, {})
            else
                SpringGiveOrderToUnit(unitGroup[i], cmdID, targetPosition:AsSpringVector(), {})
            end
        end
    end

    if (groupDistanceFromChief > 100*#unitGroup) then
        SpringGiveOrderToUnit(chief, stopID, {}, {})
    else
        SpringGiveOrderToUnit(chief, cmdID, targetPosition:AsSpringVector(), {})
    end
    return RUNNING
end

function Reset(self)
	ClearState(self)
end
