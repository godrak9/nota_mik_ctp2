local sensorInfo = {
	name = "HillLocations",
	desc = "Returns list of locations with maximum height",
	author = "PavleMikus",
	date = "",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- change is not expected

local groundHeight = Spring.GetGroundHeight
local area = 10000
local areaThreshold = 200
local mapX = Game.mapSizeX
local mapZ = Game.mapSizeZ

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function getDistance(x1,z1, x2,z2)
    local diffx = math.abs(x1-x2)
    local diffz = math.abs(z1-z2)
    return math.sqrt(diffx*diffx+diffz*diffz)
end

return function()
    local min, max = Spring.GetGroundExtremes()
    locations = {}
    for x=1,mapX do
        for z=1,mapZ do
            local height = groundHeight(x,z)
            if (height >= max-2) then   --WTF? without -1, the function will freeze on call, with -1, it works as expected.
                local done = false
                for i=1,#locations do
                    local dis = getDistance(locations[i].x, locations[i].z, x,z);
                    if (dis < areaThreshold) then
                        locations[i].x = locations[i].x*(area-1)/area + x*1/area
                        locations[i].z = locations[i].z*(area-1)/area + z*1/area
                        locations[i].y = max-1
                        done = true
                        break
                    end
                end
                if (done == false) then
                    locations[#locations+1] = {x=x,z=z}
                end
            end

        end
    end

    return locations

end
